export const ISSUER = 'https://auth-test.ceda.ac.uk/auth/realms/esgf';
export const CLIENTID = process.env.CLIENT_ID || '';
export const CLIENTSECRET = process.env.CLIENT_SECRET || '';
