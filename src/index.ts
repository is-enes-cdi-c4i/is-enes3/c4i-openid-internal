import { Issuer } from 'openid-client';

import express from 'express';

import redis from 'redis';
import { promisify } from 'util';
import { ISSUER, CLIENTID, CLIENTSECRET } from './config';

const redisClient = redis.createClient({ host: 'redis', port: 80 });
// eslint-disable-next-line @typescript-eslint/unbound-method
const getAsync = promisify(redisClient.get).bind(redisClient);

const main = async (): Promise<void> => {
  const issuer = await Issuer.discover(ISSUER);
  const client = new issuer.Client({
    client_id: CLIENTID,
    client_secret: CLIENTSECRET,
    response_types: ['code'],
  });

  const app = express();
  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  app.get('/access_token/:sessionID', async (req, res) => {
    try {
      const b64sessionID =
        req.params.sessionID +
        '='.repeat((4 - (req.params.sessionID.length % 4)) % 4);
      const session = Buffer.from(b64sessionID, 'base64').toString();
      const refreshToken = await getAsync(`refresh:${session}`);
      if (refreshToken) {
        const tokenSet = await client.grant({
          grant_type: 'refresh_token',
          refresh_token: refreshToken,
        });
        res.send(tokenSet.access_token);
      } else {
        res.sendStatus(401);
      }
    } catch (err) {
      console.error(err);
      res.sendStatus(500);
    }
  });
  app.listen(4000, () => console.log('App listening...'));
};

main().catch((err) => console.error(err));
